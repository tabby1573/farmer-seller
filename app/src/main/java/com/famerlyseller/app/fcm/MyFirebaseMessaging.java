package com.famerlyseller.app.fcm;


public class MyFirebaseMessaging{
       // extends FirebaseMessagingService {
/*
    private NotificationManagerCompat notificationManager;
    String notification1, title, body, click_action;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("TAG", "onNewToken: s: " + s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {

            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                Log.e("TAG", "onComplete: task : " + task.toString());
                if (!task.isSuccessful()) {
                    return;
                }
                Log.e("TAG", "onComplete: Task " + task);
                String token = task.getResult().getToken();
                Log.e("TAG", "onComplete: " + token);
                FCMData.getInstance().setFCMToken(token);
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("TAG", "onMessageReceived: content : " + remoteMessage.toString());
//        String click_action = remoteMessage.getNotification().getClickAction();
        notificationManager = NotificationManagerCompat.from(TonsApplication.appContext);

        try {
            notification1 = remoteMessage.getData().toString();
            Log.d("TAG", "onMessageReceived: notification text : " + notification1);

            JSONObject object = new JSONObject(remoteMessage.getData().toString());
            JSONObject message = object.getJSONObject("message");
            title = message.getString("title");
            body = message.getString("body");
            click_action = message.getString("click_action");
            String intent_value = message.getString("intent_value");
            int completeJob_id = message.getInt("completeJob_id");
            String completeJob_service_name = message.getString("completeJob_service_name");


            Intent intent = new Intent(click_action);
            intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

            // Create the TaskStackBuilder and add the intent, which inflates the back stack
            // Intent resultIntent = new Intent(BaseApplication.appContext,ScrHome.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(TonsApplication.appContext);
            stackBuilder.addNextIntentWithParentStack(new Intent(TonsApplication.appContext, ScrSplash.class));

            PendingIntent pendingIntent = PendingIntent.getActivity(TonsApplication.appContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(TonsApplication.appContext, "channel1")
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setAutoCancel(true)
                    .setPriority(PRIORITY_HIGH)
                    .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                    .setContentIntent(pendingIntent)
                    .setDefaults(DEFAULT_SOUND)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("channel1", "Default channel", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(999, builder.build());

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG", "onMessageReceived: Exception " + e.getMessage());
        }
    }*/
}
