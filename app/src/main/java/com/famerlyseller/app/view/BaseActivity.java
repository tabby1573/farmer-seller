package com.famerlyseller.app.view;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iniRetrofit();
        initUI();
    }


    public abstract void iniRetrofit();

    public abstract void initUI();


}