package com.famerlyseller.app.view;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.famerlyseller.app.R;
import com.famerlyseller.app.utils.CustomIntent;

public class ScrSplash extends BaseActivity {

    private static int TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }
        setContentView(R.layout.activity_scr_splash);
    }


    @Override
    public void initUI() {
        Log.d("TAG", "initUI: called");
    }

    @Override
    public void iniRetrofit() {
        Log.d("TAG", "iniRetrofit: called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        startNext();
    }

    public void startNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CustomIntent.startActivity(ScrSplash.this, ScrLogin.class, true);
            }
        }, TIME);
    }
}