package com.famerlyseller.app.view;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.famerlyseller.app.R;

public class ScrRegister extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
               // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }
        setContentView(R.layout.activity_scr_register);
    }

    @Override
    public void iniRetrofit() {

    }

    @Override
    public void initUI() {

    }

}