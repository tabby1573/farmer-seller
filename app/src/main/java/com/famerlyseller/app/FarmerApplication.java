package com.famerlyseller.app;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.famerlyseller.app.reciever.ConnectivityReceiver;


public class FarmerApplication extends MultiDexApplication {

    public static Context appContext;

    public static final String CHANNEL_ID = "trackinglocation";


    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        createNotificationChannel();
    }

    public static synchronized FarmerApplication getInstance() {
        return (FarmerApplication) appContext;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
        Log.d("TAG", "setConnectivityListener: creating instance ");
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Tracking Location",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
