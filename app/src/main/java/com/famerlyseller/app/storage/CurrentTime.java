package com.famerlyseller.app.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.famerlyseller.app.FarmerApplication;


public class CurrentTime {

    public static CurrentTime _instance = null;
    private static SharedPreferences _sharedpreference = null;
    private static SharedPreferences.Editor _sharedprefEditor = null;
    private final String SHARED_PREF_NAME = "current_time";

    private final String KEY_CURRENT_TIME = "CURRENT_TIME";

    private CurrentTime() {

    }

    public static CurrentTime getInstance() {

        if (_instance == null) {
            _instance = new CurrentTime();
            _instance.iniSharedPref();
        }
        return _instance;
    }

    public void iniSharedPref() {
        _sharedpreference = get_sharedpreference();
        _sharedprefEditor = get_sharedprefEditor();
    }

    private SharedPreferences get_sharedpreference() {
        if (_sharedpreference == null) {
            _sharedpreference = FarmerApplication.appContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return _sharedpreference;
    }

    private SharedPreferences.Editor get_sharedprefEditor() {
        if (_sharedprefEditor == null) {
            _sharedprefEditor = get_sharedpreference().edit();
        }
        return _sharedprefEditor;
    }

    public void setKEY_CURRENT_TIME(String current_time) {
        _sharedprefEditor.putString(KEY_CURRENT_TIME, current_time).commit();
    }

    public String getKEY_CURRENT_TIME() {
        return _sharedpreference.getString(KEY_CURRENT_TIME, null);
    }
}
