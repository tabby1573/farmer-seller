package com.famerlyseller.app.storage;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.famerlyseller.app.FarmerApplication;


public class FCMData {

    private static FCMData _instance = null;
    private static SharedPreferences _sharedPreferences = null;
    private static Editor _sharedPrefEditor = null;
    private final String SHARED_PREFERENCE_NAME = "fcm_data";

    private final String KEY_FCM_TOKEN = "FCM_TOKEN";
    private final String KEY_FCM_TOKEN_SENT = "FCM_TOKEN_SENT";

    /**
     * Private Constructor
     */
    private FCMData() {

    }

    public static FCMData getInstance() {
        if (_instance == null) {
            _instance = new FCMData();
            _instance._initSharedPreferences();
        }
        return _instance;
    }

    /**
     * This method is used to initialized {@link SharedPreferences} and
     * {@link Editor}
     */
    private void _initSharedPreferences() {
        _sharedPreferences = _getSharedPref();
        _sharedPrefEditor = _getSharedPrefEditor();
    }

    /**
     * Method to get the SharedPreferences.
     *
     * @return the {@link SharedPreferences} object.
     */
    private SharedPreferences _getSharedPref() {
        if (_sharedPreferences == null) {
            _sharedPreferences = FarmerApplication.appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return _sharedPreferences;
    }

    /**
     * Method to get the {@link Editor} for writing values to {@link SharedPreferences}.
     *
     * @return the {@link Editor} object.
     */
    private Editor _getSharedPrefEditor() {
        if (_sharedPrefEditor == null) {
            _sharedPrefEditor = _getSharedPref().edit();
        }
        return _sharedPrefEditor;
    }


    public void setFCMToken(String token) {
        _getSharedPrefEditor().putString(KEY_FCM_TOKEN, token).commit();
    }

    public String getFCMToken() {
        return _getSharedPref().getString(KEY_FCM_TOKEN, null);
    }

    public void setFCMTokenSent(boolean isSent) {
        _getSharedPrefEditor().putBoolean(KEY_FCM_TOKEN_SENT, isSent).commit();
    }

    public boolean isFCMTokenSent() {
        return _getSharedPref().getBoolean(KEY_FCM_TOKEN_SENT, true);//
    }

}
