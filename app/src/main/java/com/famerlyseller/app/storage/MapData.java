package com.famerlyseller.app.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.famerlyseller.app.FarmerApplication;


public class MapData {

    public static MapData _instance = null;
    private static SharedPreferences _sharedpreference = null;
    private static SharedPreferences.Editor _sharedprefEditor = null;

    private final String SHARED_PREF_NAME = "map_data";

    private final String ADDRESS = "address";
    private final String LAT = "lat";
    private final String LNG = "lng";

    private MapData() {

    }

    public static MapData getInstance() {

        if (_instance == null) {
            _instance = new MapData();
            _instance.iniSharedPref();
        }
        return _instance;
    }

    public void iniSharedPref() {
        _sharedpreference = get_sharedpreference();
        _sharedprefEditor = get_sharedprefEditor();
    }

    private SharedPreferences get_sharedpreference() {
        if (_sharedpreference == null) {
            _sharedpreference = FarmerApplication.appContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return _sharedpreference;
    }

    private SharedPreferences.Editor get_sharedprefEditor() {
        if (_sharedprefEditor == null) {
            _sharedprefEditor = get_sharedpreference().edit();
        }
        return _sharedprefEditor;
    }

    public void setADDRESS(String address) {
        _sharedprefEditor.putString(ADDRESS, address).commit();
    }

    public String getADDRESS() {
        return _sharedpreference.getString(ADDRESS, null);
    }

    public void setLAT(String address) {
        _sharedprefEditor.putString(LAT, address).commit();
    }

    public String getLAT() {
        return _sharedpreference.getString(LAT, null);
    }

    public void setLNG(String address) {
        _sharedprefEditor.putString(LNG, address).commit();
    }

    public String getLNG() {
        return _sharedpreference.getString(LNG, null);
    }

    public void clearData() {
        setADDRESS(null);
        setLAT(null);
        setLNG(null);
    }
}
