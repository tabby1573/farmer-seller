package com.famerlyseller.app.storage;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.famerlyseller.app.FarmerApplication;


public class UserData {

    private static UserData _instance = null;
    private static SharedPreferences _sharedPreferences = null;
    private static Editor _sharedPrefEditor = null;
    private final String SHARED_PREFERENCE_NAME = "user_data";


    private final String NEW_USER = "NEW_USER";
    private final String TOKEN = "TOKEN";
    private final String NAME = "NAME";
    private final String MOBILE = "MOBILE";
    private final String LOCATION_LAT = "LOCATION_LAT";
    private final String LOCATION_LANG = "LOCATION_LANG";
    private final String LOCATION_TEXT = "LOCATION_TEXT";
    private final String LOCATION_ID = "LOCATION_ID";


    private UserData() {

    }

    public static UserData getInstance() {
        if (_instance == null) {
            _instance = new UserData();
            _instance._initSharedPreferences();
        }
        return _instance;
    }

    /**
     * This method is used to initialized {@link SharedPreferences} and
     * {@link Editor}
     */
    public void _initSharedPreferences() {
        _sharedPreferences = _getSharedPref();
        _sharedPrefEditor = _getSharedPrefEditor();
    }

    /**
     * Method to get the SharedPreferences.
     *
     * @return the {@link SharedPreferences} object.
     */
    private SharedPreferences _getSharedPref() {
        if (_sharedPreferences == null) {
            _sharedPreferences = FarmerApplication.appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return _sharedPreferences;
    }

    /**
     * Method to get the {@link Editor} for writing values to {@link SharedPreferences}.
     *
     * @return the {@link Editor} object.
     */
    private Editor _getSharedPrefEditor() {
        if (_sharedPrefEditor == null) {
            _sharedPrefEditor = _getSharedPref().edit();
        }
        return _sharedPrefEditor;
    }


    public void setNewUser(boolean newUser) {
        _getSharedPrefEditor().putBoolean(NEW_USER, newUser).commit();
    }

    public boolean isNewUser() {
        return _getSharedPref().getBoolean(NEW_USER, false);
    }

    public void setTOKEN(String id) {
        _getSharedPrefEditor().putString(TOKEN, id).commit();
    }

    public String getTOKEN() {
        return _getSharedPref().getString(TOKEN, null);
    }

    public void setName(String id) {
        _getSharedPrefEditor().putString(NAME, id).commit();
    }

    public String getName() {
        return _getSharedPref().getString(NAME, null);
    }

    public void setMobile(String id) {
        _getSharedPrefEditor().putString(MOBILE, id).commit();
    }

    public String getMobile() {
        return _getSharedPref().getString(MOBILE, null);
    }

    public void setLocationLat(String id) {
        _getSharedPrefEditor().putString(LOCATION_LAT, id).commit();
    }

    public String getLocationLat() {
        return _getSharedPref().getString(LOCATION_LAT, null);
    }

    public void setLocationLng(String id) {
        _getSharedPrefEditor().putString(LOCATION_LANG, id).commit();
    }

    public String getLocationLng() {
        return _getSharedPref().getString(LOCATION_LANG, null);
    }

    public void setLocationText(String id) {
        _getSharedPrefEditor().putString(LOCATION_TEXT, id).commit();
    }

    public String getLocationText() {
        return _getSharedPref().getString(LOCATION_TEXT, null);
    }

    public void setStoreId(int id) {
        _getSharedPrefEditor().putInt(LOCATION_ID, id).commit();
    }

    public int getStoreId() {
        return _getSharedPref().getInt(LOCATION_ID, 0);
    }


    public void clearData() {
      /*  setNewUser(false);
        setTOKEN(null);
        setMobile(null);
        setName(null);*/
        _getSharedPrefEditor().clear();
        _getSharedPrefEditor().commit();
    }


}
