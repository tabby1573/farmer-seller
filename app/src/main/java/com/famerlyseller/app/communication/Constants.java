package com.famerlyseller.app.communication;

public class Constants {

    public static final String PROGRESS_MSG = "Please Wait...";
    public static final String WENT_WRONG = "Something went wrong please try again later";
    public static final String FETCH_ERROR = "Data fetch error";
    public static final String SERVER_ERROR = "Server error";
    public static final String FETCH_CURRENT_LOCATION_MSG = "Fetching Location...";

    public static final String ID_PROOF = "ID_PROOF";
    public static final String LICENSE_FRONT = "LICENSE_FRONT";
    public static final String LICENSE_BACK = "LICENSE_BACK";

    public static final String REQ_REQUESTED = "REQ_REQUESTED";
    public static final String REQ_ONGOING = "REQ_ONGOING";
    public static final String REQ_COMPLETED = "REQ_COMPLETED";

    public static final String ASS_RECIEVED = "ASS_RECIEVED";
    public static final String ASS_REJECTED = "ASS_REJECTED";
    public static final String ASS_COMPLETED = "ASS_COMPLETED";


    public static final int USER_ACTIVE = 1;
    public static final int USER_INACTIVE = 2;

    public static final int ADDED_IN_WISHLIST = 1;
    public static final int NOT_ADDED_IN_WISHLIST = 0;

    public static final int JOB_STARTED = 1;
    public static final int JOB_NOT_STARTED = 0;

    public static final int JOB_RATED = 1;
    public static final int JOB_NOT_RATED = 0;

    public static final int CUSTOMER = 0;
    public static final int LABOUR_PENDING = 1;
    public static final int LABOUR_APPROVED = 2;
    public static final int LABOUR_REJECTED = 3;

    public static final int PAY_CASH = 1;
    public static final int PAY_ONLINE = 2;
    public static final int MODE3 = 3;


    //intent click action types
    public static final String CLICK_ACTION_NOTIFICATION = "app.tonsonline.com.NOTIFICATION";
    public static final String CLICK_ACTION_HOME = "app.tonsonline.com.HOME";
    public static final String CLICK_ACTION_COMPLETED_JOB_DETAILS_FOR_LABOUR = "app.tonsonline.com.COMPLETED_JOB_DETAILS_FOR_LABOUR";
    public static final String CLICK_ACTION_COMPLETED_JOB_DETAILS_FOR_CUSTOMER = "app.tonsonline.com.COMPLETED_JOB_DETAILS_FOR_CUSTOMER";
    public static final String CLICK_ACTION_COMPLETED_JOB_DETAILS_FOR_CUSTOMER_VEHICLE = "app.tonsonline.com.COMPLETED_JOB_DETAILS_FOR_CUSTOMER_VEHICLE";
    public static final String CLICK_ACTION_COMPLETED_JOB_DETAILS_FOR_LABOUR_VEHICLE = "app.tonsonline.com.COMPLETED_JOB_DETAILS_FOR_LABOUR_VEHICLE";
    public static final String CLICK_ACTION_COMPLETED_PREVIUOS_JOBS = "app.tonsonline.com.PREVIOUS_JOBS";
    public static final String CLICK_ACTION_COMPLETED_PREVIUOS_JOBS_VEHICLE = "app.tonsonline.com.PREVIOUS_JOBS_VEHICLE";

    // intent constant
    public static final String JOB_BOOKED = "JOB_BOOKED";
    public static final String ASSIGNED_COMPLETED = "ASSIGNED_COMPLETED";
    public static final String REQUESTED_COMPLETED = "REQUESTED_COMPLETED";

    // notification constants
    public static final String AcceptJob = "AcceptJob";  //it should go to ongoing tab
    public static final String BookJob = "BookJob";  //it should go to assigned received jobs
    public static final String StartHome = "Home";  //it should go to Home only
    public static final String StartJob = "StartJob";  //it should go to assigned ongoing job
    public static final String RejectJob = "RejectJob";  //it should go to home
    public static final String AcceptExtend = "AcceptExtend";  //it should go to requested ongoing job
    public static final String RejectExtend = "RejectExtend";  //it should go to requested ongoing job
    public static final String CancelJob = "CancelJob";  //it should go to requested ongoing job
    public static final String RequestExtend = "RequestExtend";  //it should go to requested ongoing job


    //wallet mode constants
    public static final int W_GREEN = 0;
    public static final int W_RED = 1;
    public static final int W_GREY = 2;

    public static final String W_EARNED = "Earned";
    public static final String W_SPENT = "Redeemed";
    public static final String W_EXPIRED = "Expired";

    //wallet mod =0 green, wallet mod =1 red,  wallet mod =2 grey,
    //also text will be earned, spent , expired respectively

    //empty layout
    public static final String EMPTY_LAYOUT = "EMPTY_LAYOUT";

    //bottom appbar for home
    public static final int HOME = 1;
    public static final int ASSIGNED = 2;
    public static final int REQUESTED = 3;
    public static final int PROFILE = 4;
    public static final int SEARCH = 5;

}
