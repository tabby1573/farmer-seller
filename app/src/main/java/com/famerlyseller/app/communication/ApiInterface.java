package com.famerlyseller.app.communication;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {


    // login related
    @GET("login")
    Call<String> login(@Query("mobile_no") String mobile_no, @Query("name") String name, @Query("user_token") String user_token);

    @GET("checkOtp")
    Call<String> checkOtp(@Query("mobile_no") String mobile_no, @Query("otp") String otp);

    @GET("checkOtp1")
    Call<String> checkOtp1(@Query("mobile_no") String mobile_no, @Query("otp") String otp);

    @GET("referralCode")
    Call<String> referral_code(@Query("referral_code") String referral_code, @Header("Token") String Token);


    @GET("changeMobilenumber")
    Call<String> changeMobilenumber(@Query("mobile_no") String mobile_no, @Header("Token") String Token);

    @GET("resend_otp")
    Call<String> resend_otp(@Query("mobile_no") String mobile_no);

    @GET("locationSubmit")
    Call<String> locationSubmit(@Header("Token") String Token, @QueryMap Map<String, String> params);

    @GET("exitApp")
    Call<String> exitApp(@Header("Token") String Token);

    @GET("logout")
    Call<String> logout(@Header("Token") String Token);

    // home
    @GET("home")
    Call<String> home(@Header("Token") String Token);

    @GET("homeNew")
    Call<String> homeNew(@Header("Token") String Token, @Query("user_token") String user_token);

    @GET("acceptJob")
    Call<String> acceptJob(@Query("job_id") String job_id);

    @GET("rejectJob")
    Call<String> rejectJob(@Query("job_id") String job_id);

    @GET("updateAvailability")
    Call<String> updateAvailability(@Header("Token") String Token, @Query("availability") int availability);

    //category and sub-category
    @GET("listAllcategory")
    Call<String> listAllcategory();

    @GET("listAllSubcategory")
    Call<String> listAllSubcategory(@Query("categoryId") String categoryId, @Header("Token") String Token);


    //add to wish list and remove
    @GET("addTowishlist")
    Call<String> addTowishlist(@Header("Token") String Token, @Query("subCategoryId") String subCategoryId);

    @GET("removeFavourites")
    Call<String> removeFavourites(@Query("wish_id") String wish_id);

    @GET("myFavourites")
    Call<String> myFavourites(@Header("Token") String Token);

    //popular services
    @GET("popularServices")
    Call<String> popularServices();

    // notification
    @GET("notifications")
    Call<String> notifications(@Header("Token") String id);

    //profile
    @GET("myProfile")
    Call<String> myProfile(@Header("Token") String Token);

    @GET("econtactProfile")
    Call<String> econtactProfile(@Header("Token") String Token);

    @Multipart
    @POST("editProfile")
    Call<String> editProfile(@PartMap Map<String, String> params, @Part MultipartBody.Part file, @Part("file") RequestBody name);


    // My Requested Job
    @GET("myRequestedJobs")
    Call<String> myRequestedJobs(@Header("Token") String Token);

    @GET("ongoingJobs")
    Call<String> ongoingJobs(@Header("Token") String Token);

    @GET("completedJobs")
    Call<String> completedJobs(@Header("Token") String Token);


    // My assigned Jobs
    @GET("myAssignedJobs")
    Call<String> myAssignedJobs(@Header("Token") String Token);

    @GET("myAssignedongoingJobs")
    Call<String> myAssignedongoingJobs(@Header("Token") String Token);

    @GET("myAssignedcompletedJobs")
    Call<String> myAssignedcompletedJobs(@Header("Token") String Token);


    //start and complete job
    @GET("clickStartJob")
    Call<String> clickStartJob(@Query("job_id") String job_id);

    @GET("clickCompleteJob")
    Call<String> clickCompleteJob(@Header("Token") String Token, @Query("job_id") int job_id);

    @Multipart
    @POST("registrationForm")
    Call<String> registrationForm(@PartMap Map<String, String> params, @Part MultipartBody.Part file, @Part("file") RequestBody name, @Part MultipartBody.Part file2, @Part("file2") RequestBody name2);

    @Multipart
    @POST("registrationForm")
    Call<String> test(@Part MultipartBody.Part file, @Part("file") RequestBody name, @Part MultipartBody.Part file2, @Part("file2") RequestBody name2);

    //test api
    @Multipart
    @POST("reg")
    Call<String> reg(@PartMap Map<String, String> params, @Part MultipartBody.Part file, @Part("file") RequestBody name);

    //distric, panchayath and ward
    @GET("allDistrict")
    Call<String> allDistrict();

    @GET("allPanchayath")
    Call<String> allPanchayath(@Query("district_id") int district_id);

    @GET("allWard")
    Call<String> allWard(@Query("panchayath_id") int panchayath_id);

    @GET("serviceRequired")
    Call<String> serviceRequired(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("listLabours")
    Call<String> listLabours(@Header("Token") String Token, @QueryMap HashMap<String, String> params);


    //my service operations
    //@GET("addMyServices")
    @GET("addMyServices")
    Call<String> addMyServices(@Header("Token") String Token, @QueryMap Map<String, String> params);

    @GET("listServices")
    Call<String> listServices(@Header("Token") String Token);

    //@GET("updateMyservice")
    @GET("updateMyservice")
    Call<String> updateMyservice(@Header("Token") String Token, @QueryMap Map<String, String> params);

    @GET("deleteMyservice")
    Call<String> deleteMyservice(@Query("serviceId") int serviceId);

    //@GET("viewSingleservice")
    @GET("viewSingleservicemain")
    Call<String> viewSingleservice(@Query("serviceId") int serviceId);

    @GET("searchSubcategory")
    Call<String> searchSubcategory(@Header("Token") String Token, @Query("search_key") String search_key);

    @GET("singleLabour")
    Call<String> singleLabour(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("bookJob")
    Call<String> bookJob(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("acceptJob")
    Call<String> acceptJob(@Query("job_id") int job_id);

    @GET("rejectJob")
    Call<String> rejectJob(@Query("job_id") int job_id);

    @GET("rateJob")
    Call<String> rateJob(@Header("Token") String Token, @QueryMap Map<String, String> params);

    @GET("mycompletedJobDetails")
    Call<String> mycompletedJobDetails(@Query("job_id") int job_id); // for labour completed job details

    @GET("completedJobDetails")
    Call<String> completedJobDetails(@Query("job_id") int job_id);// for customer completed job details

    @GET("confirmPayment")
    Call<String> confirmPayment(@Query("job_id") int job_id, @Query("mod") int mod);// for customer completed job details

    @GET("feedback")
    Call<String> feedback(@Header("Token") String job_id, @Query("feedback") String feedback);

    @GET("canceljob")
    Call<String> canceljob(@Query("job_id") int job_id);

    @GET("listSubcategory")
    Call<String> listSubcategory(@Header("Token") String id);

    @GET("faq")
    Call<String> faq();

    @GET("aboutUs")
    Call<String> aboutUs();

    @GET("extendHours")
    Call<String> extendHours();

    @GET("submitExtend")
    Call<String> submitExtend(@Query("job_id") int job_id, @Query("number") String number);

    @GET("acceptExtend")
    Call<String> acceptExtend(@Query("job_id") int job_id);

    @GET("rejectExtend")
    Call<String> rejectExtend(@Query("job_id") int job_id);


    //-------------------------new API--------------------------
    @GET("vehicleServices")
    Call<String> vehicleServices();

    @GET("listHomeServices")
    Call<String> listHomeServices();

    @GET("myreferralCode")
    Call<String> myreferralCode(@Header("Token") String Token);

    @GET("myWallet")
    Call<String> myWallet(@Header("Token") String Token);

    @GET("transactionHistory")
    Call<String> transactionHistory(@Header("Token") String Token);

    @GET("listServices")
    Call<String> listServicesProfile(@Header("Token") String Token);

    @GET("changeServiceAvailability")
    Call<String> changeServiceAvailability(@Query("serviceId") int serviceId, @Query("availability") int availability);

    @GET("serviceInstruction")
    Call<String> serviceInstruction(@Query("subCategoryId") int subCategoryId);

    @GET("allBanners")
    Call<String> allBanners();

    @GET("showallVehicleCategories")
    Call<String> showallVehicleCategories(@Header("Token") String Token);

    @GET("showallOtherCategories")
    Call<String> showallOtherCategories(@Header("Token") String Token);

    @GET("addmoreFavorite")
    Call<String> addmoreFavorite(@Header("Token") String Token);

    @GET("vehicleLabourSearch")
    Call<String> vehicleLabourSearch(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("listVehicleLabours")
    Call<String> listVehicleLabours(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("vehiclebookJob")
    Call<String> vehiclebookJob(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("singlevehicleLabour")
    Call<String> singlevehicleLabour(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("myQrcode")
    Call<String> myQrcode(@Header("Token") String Token);

    @GET("redeemPoint")
    Call<String> redeemPoint(@QueryMap HashMap<String, String> params);

    @GET("qrcodeScanning")
    Call<String> qrcodeScanning(@Query("qrcode") String qrcode);

    @GET("termsCondition")
    Call<String> termsCondition();

    @GET("bookService")
    Call<String> bookService(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("bookServiceVehicle")
    Call<String> bookServiceVehicle(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("editLocation")
    Call<String> editLocation(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("searchPreviouslabourList")
    Call<String> searchPreviouslabourList(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("addMyliveLocation")
    Call<String> addMyliveLocation(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("myliveLocation")
    Call<String> myliveLocation(@Query("labour_id") int labor_id);

    @GET("contactdetails")
    Call<String> contactdetails();

    @GET("allState")
    Call<String> allState();

    @GET("updateBankDetails")
    Call<String> updateBankDetails(@Header("Token") String Token, @QueryMap HashMap<String, String> params);

    @GET("emergencyList")
    Call<String> emergencyList();
}