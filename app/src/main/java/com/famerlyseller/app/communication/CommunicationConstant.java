package com.famerlyseller.app.communication;

public class CommunicationConstant {

    public static final String ROOT_URL = "http://tonsonline.com/tons/";

    //public static final String BASE_URL = ROOT_URL + "Api/";
    //public static final String BASE_URL = ROOT_URL + "Android_Api/";
    public static final String BASE_URL = ROOT_URL + "Android_Api_v2/";

    public static final String BASE_IMAGE_URL = ROOT_URL + "uploads/";
    public static final String PROFILE_IMAGE_URL = BASE_IMAGE_URL + "photo/";
    public static final String ID_PROOF_IMAGE_URL = BASE_IMAGE_URL + "proof/";
    public static final String QR_CODE_IMAGE_URL = BASE_IMAGE_URL + "qr_image/";

    public static final String REGISTRATION_URL = BASE_URL + "registrationFormNew";
    public static final String UPDATE_PROFILE_URL = BASE_URL + "editProfile";
    public static final String UPLOAD_LICENSE_URL = BASE_URL + "updateLicense";

    public static final String PAYMENT_URL = "https://www.tonsonline.com/tons/Tons/add_payment/";

}

