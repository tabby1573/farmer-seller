package com.famerlyseller.app.utils;

import android.util.Log;
import android.widget.TextView;

import com.famerlyseller.app.R;
import com.famerlyseller.app.FarmerApplication;
import com.famerlyseller.app.storage.CurrentTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import static android.view.View.VISIBLE;

public class SetCountDownTimer {

    public static String upcomingTimerCalculation(TextView txtCurrentTime, String start_quiz_time) {

        String countDownTime = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {

                futureDate = dateFormat.parse(CurrentTime.getInstance().getKEY_CURRENT_TIME());
                futureDate1 = dateFormat.parse(start_quiz_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                countDownTime = String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
                txtCurrentTime.setTextColor(FarmerApplication.appContext.getResources().getColor(R.color.text_black));
                txtCurrentTime.setText(countDownTime);
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    countDownTime = String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    txtCurrentTime.setTextColor(FarmerApplication.appContext.getResources().getColor(R.color.text_black));
                    txtCurrentTime.setText(countDownTime);
                } else {
                    txtCurrentTime.setVisibility(VISIBLE);
                    txtCurrentTime.setText("Finished!");

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG", "upcomingTimerCalculation: Exception " + e.getMessage());
        }

        return countDownTime;
    }
}
